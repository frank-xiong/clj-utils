# clj-utils

[![pipeline status](https://gitlab.com/frank-xiong/clj-utils/badges/dev/pipeline.svg)](https://gitlab.com/frank-xiong/clj-utils/-/commits/dev)
[![coverage report](https://gitlab.com/frank-xiong/clj-utils/badges/dev/coverage.svg)](https://gitlab.com/frank-xiong/clj-utils/-/commits/dev)

#### test
```shell
clojure -Sdeps '{:mvn/local-repo ".m2"}' -A:test
```

#### package
```shell
clojure -Sdeps '{:mvn/local-repo ".m2"}' -A:uberdeps --deps-file deps.edn --target target/clj-utils.jar
```

## Author
FrankXiong 2021-12

## License
Copyright FrankXiong (2021), All rights reserved.
