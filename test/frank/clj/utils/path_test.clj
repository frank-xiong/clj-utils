(ns frank.clj.utils.path-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]
            [frank.clj.utils.core :as u.core]
            [frank.clj.utils.path :as sut]))

(deftest is-specified-file?
  (testing "test if the file is valid"
    (is (true? (sut/is-specified-file? (io/file "project.edn") ".edn")))
    (is (false? (sut/is-specified-file? (io/file "project.edn") ".xxx")))
    (is (false? (sut/is-specified-file? (io/file "product.edn") ".edn")))))

(deftest with-path
  (testing "test operate for each file"
    (is (= #{"deps.edn" "project.edn"}
           (set (sut/with-path "."
                               #(sut/is-specified-file? % ".edn")
                               #(.getName %)))))
    (is (= 'frank/clj-utils
           (sut/with-path "project.edn"
                          #(sut/is-specified-file? % ".edn")
                          (fn [f] (:name (read-string (slurp f)))))))
    (is (= ["invalid path"
            {:reason :invalid-path :file "product.edn"}]
           (try
             (sut/with-path "product.edn"
                            #(sut/is-specified-file? % ".edn")
                            identity)
             (catch Exception e [(ex-message e) (ex-data e)]))))))

(deftest delete-path
  (testing "test delete path"
    (let [files ["./resources/output.edn"
                 "./resources/output/a.edn"
                 "./resources/output/b.edn"]]
      (.mkdirs (io/file "./resources/output"))
      (dotimes [i (count files)] (spit (files i) i))
      (is (= 5 (sut/delete-path (io/file "./resources"))))
      (is (= 0 (sut/delete-path (io/file "./resources")))))))

(deftest relative-path
  (testing "test get relative path"
    (is (= "."
           (sut/relative-path "./src" "./src/frank/clj/../..")))
    (is (= "frank/clj"
           (sut/relative-path "./src" "./src/frank/clj/")))
    (is (= "frank/clj/utils/path.clj"
           (sut/relative-path "./src" "./src/frank/clj/utils/path.clj")))
    (is (= "frank/clj/utils/path.clj"
           (sut/relative-path "./src" "./src/frank/clj/../../frank/clj/utils/path.clj")))
    (is (= "frank/clj/utils/path.clj"
           (sut/relative-path "./src/frank/clj/../.." "./src/frank/clj/utils/path.clj")))
    (is (= "xx.edn"
           (sut/relative-path "./src/frank/clj/no-exist.clj" "./src/frank/clj/no-exist.clj/xx.edn")))
    (is (nil? (sut/relative-path "./src/frank/clj/utils/path.clj"
                                 "./src/frank/clj/utils/path.clj/xx.edn")))
    (is (nil? (sut/relative-path "./src/frank/clj/utils/"
                                 "./test/frank/clj/utils/path-test.clj")))))

(deftest pretty-write-to-file
  (testing "test print data to file"
    (let [data [{:user/id 1 :user/name "Frank"}
                {:user/id 3 :user/name "Allen"}
                {:user/id 2 :user/name "Helen"}]
          output-path "./resources/output.edn"]
      (let [path (sut/pretty-write-to-file data output-path
                                           {:comparator (fn [a b] (compare (:user/id b) (:user/id a)))
                                            :width      32
                                            :zp-option  {:map {:comma?    false
                                                               :key-order [:user/id]}}})
            edn (u.core/file->edn path)]
        (is (= path output-path))
        (is (vector? edn))
        (is (= [{:user/id 3 :user/name "Allen"}
                {:user/id 2 :user/name "Helen"}
                {:user/id 1 :user/name "Frank"}]
               edn)))
      (let [path (sut/pretty-write-to-file (apply list data)
                                           output-path
                                           {:comparator (fn [a b] (compare (:user/id a) (:user/id b)))
                                            :zp-option  {:map {:comma?    false
                                                               :key-order [:user/id]}}})
            edn (u.core/file->edn path)]
        (is (= path output-path))
        (is (list? edn))
        (is (= [{:user/id 1 :user/name "Frank"}
                {:user/id 2 :user/name "Helen"}
                {:user/id 3 :user/name "Allen"}]
               edn)))
      (sut/delete-path (io/file "./resources")))))
