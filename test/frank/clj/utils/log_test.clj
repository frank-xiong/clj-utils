(ns frank.clj.utils.log-test
  (:require [clojure.test :refer :all]
            [frank.clj.utils.log :as sut]))

(deftest log
  (testing "test log"
    (is (nil? (sut/log :info {:msg "msg"})))
    (is (nil? (sut/log {:msg "msg"})))
    (is (nil? (sut/log :debug {:msg "msg"})))
    (is (nil? (sut/log :error (ex-info "msg" {:msg "msg"}) "occur exception:")))))

(deftest show-value
  (testing "test show value"
    (is (= {:a 1} (sut/show-value {:a 1})))
    (is (= {:a 1} (sut/show-value "msg:" {:a 1})))))
