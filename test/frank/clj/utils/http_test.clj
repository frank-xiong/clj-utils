(ns frank.clj.utils.http-test
  (:require [clojure.test :refer :all]
            [frank.clj.utils.http :as sut]))

(deftest make-req
  (testing "test make request"
    (is (= {:url    "https://clojure.org"
            :method :post
            :body   "{}"
            :as     :clojure}
           (sut/make-req nil "https://clojure.org" {})))
    (is (= {:url    "https://clojure.org"
            :method :put
            :as     :clojure}
           (sut/make-req :put "https://clojure.org" nil)))))

(deftest show-response
  (testing "test show response"
    (is (= "body"
           (sut/show-response {:status 200 :body "body"})))
    (is (nil? (sut/show-response {:status 404 :body "error"})))))
