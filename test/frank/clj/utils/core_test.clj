(ns frank.clj.utils.core-test
  (:require [clojure.test :refer :all]
            [frank.clj.utils.core :as sut])
  (:import (java.time ZoneId)))

(deftest get-id
  (testing "test get id"
    (is (= 1 (sut/get-id)))
    (is (= 2 (sut/get-id)))
    (is (= 3 (sut/get-id)))))

(deftest file->edn
  (testing "test read edn from file"
    (is (= 'frank/clj-utils
           (:name (sut/file->edn "project.edn"))))))

(deftest split-n
  (testing "testing split a coll"
    (is (= [[0 1 2 3 4] [5 6 7 8 9]]
           (sut/split-n (range 10) 2)))
    (is (= [[0 1 2 3] [4 5 6 7] [8 9]]
           (sut/split-n (range 10) 3)))
    (is (= [[0 1 2] [3 4 5] [6 7 8] [9]]
           (sut/split-n (range 10) 4)))
    (is (= [[0 1] [2 3] [4 5] [6 7] [8 9]]
           (sut/split-n (range 10) 5)))))

(deftest md5-string
  (testing "MD5 for string"
    (is (= "d41d8cd98f00b204e9800998ecf8427e"
           (sut/md5-string "")))
    (is (= "7215ee9c7d9dc229d2921a40e899ec5f"
           (sut/md5-string " ")))
    (is (= "37a1fa046b364670fe8929f29f22d4dc"
           (sut/md5-string "clj/utils")))))

(deftest millis->string
  (testing "convert milliseconds to string"
    (is (= "2021-01-01 00:00:00.000"
           (sut/millis->string 1609430400000)))
    (is (= "2021-01-01 00:00:00"
           (sut/millis->string 1609430400001
                               "yyyy-MM-dd HH:mm:ss"
                               (ZoneId/of "Asia/Shanghai"))))))

(deftest string->millis
  (testing "convert string to milliseconds"
    (is (= 1609430400001
           (sut/string->millis "2021-01-01 00:00:00.001")))
    (is (= 1609430400000
           (sut/string->millis "2021-01-01 00:00:00"
                               "yyyy-MM-dd HH:mm:ss"
                               (ZoneId/of "Asia/Shanghai"))))))

(deftest random
  (testing "test random range"
    (is (< (sut/random 10) 10))
    (is (< 0 (sut/random 1 10) 10))))

(deftest sort-by-keys
  (testing "test sort sequence by keys"
    (is (= [{:d 3 :b 2 :c 2}
            {:a 1 :b 3 :c 3}
            {:a 1 :b 3 :c 5}
            {:a 1 :b 4 :c 4}
            {:a 2 :b 2 :c 1}
            {:a 2 :b 4 :d 3}
            {:a 2 :b 4 :c 1}]
           (sut/sort-by-keys [{:a 1 :b 3 :c 3}
                              {:a 1 :b 4 :c 4}
                              {:a 1 :b 3 :c 5}
                              {:d 3 :b 2 :c 2}
                              {:a 2 :b 4 :d 3}
                              {:a 2 :b 4 :c 1}
                              {:a 2 :b 2 :c 1}]
                             [:a :b :c])))))

(deftest compare-sequence
  (testing "Compare all elements for equality"
    (is (= {:result :equal}
           (sut/compare-sequence [1 2 3] [1 2 3])))
    (is (= {:result :different-count :data [3 4]}
           (sut/compare-sequence [1 2 3] [1 2 3 4])))
    (is (= {:result :different-element :data [1 3 4]}
           (sut/compare-sequence [1 2 3 4 5] [1 4 3 2 7])))
    (is (= {:result :different-element :data [2]}
           (sut/compare-sequence (fn [a b] (< (+ a b) 5))
                                 [1 2 3] [1 2 3])))))

(deftest index-if
  (testing "test get index by f"
    (is (= 100 (sut/index-if (range 200) (partial = 100))))
    (is (= 100 (sut/index-if (range 200) = 100)))
    (is (= 100 (sut/index-if (range 200) (fn [a b c] (< (* b c) a)) 9 11)))
    (is (= 100 (sut/index-if (range 200) (fn [a b c d e] (< (* b c d e) a)) 3 3 11 1)))
    (is (= nil (sut/index-if (range 200) > 203)))))

(deftest index-of
  (testing "test get index of v in coll"
    (is (= 1   (sut/index-of [:a :b :c] :b)))
    (is (= nil (sut/index-of [:a :b :c] :d)))))

(deftest transduce-map
  (testing "test transform and reduce for map"
    (is (= {:a 2 :b 3 :c 4}
           (sut/transform-map {:a 1 :b 2 :c 3} inc)))
    (is (= {:a 2 :c 4}
           (sut/transform-map {:a 1 :b 2 :c 3} inc
                              (fn [k v] (or (= :a k) (< 3 v))))))))

(deftest extract-cyclic
  (testing "test circular reference"
    (let [m {:a [:1 :4]
             :b [:2]
             :c [:3]}]
      (is (= [[:c :3 :c]]
             (sut/extract-cyclic m {:1 [:d] :2 [:c] :3 [:c]})))
      (is (= [[:3 :c :3]]
             (sut/extract-cyclic {:1 [:d] :2 [:c] :3 [:c]} m)))
      (is (= []
             (sut/extract-cyclic m {:1 [:d] :2 [:a] :3 [:b]})))
      (is (= [[:c :3 :c] [:3 :c :3]]
             (sut/extract-cyclic (merge m {:1 [:d] :2 [:c] :3 [:c]})))))))
