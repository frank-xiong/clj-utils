(ns frank.clj.utils.path
  (:require [clojure.java.io :as io]
            [zprint.core :as zp]
            [frank.clj.utils.core :as u.core]))

(defn is-specified-file?
  "判定是否指定后缀的非空文件"
  [^java.io.File f suffix]
  (and (.isFile f)
       (clojure.string/ends-with? (.getName f) suffix)
       (< 0 (.length f))))

(defn with-path
  "在路径上执行相同的操作"
  [path filter-fn f]
  (let [target (io/file path)]
    (u.core/assert-ex (.exists target)
                      "invalid path"
                      {:reason :invalid-path :file path})
    (cond (.isDirectory target)
          (->> (file-seq target)
               (filter filter-fn)
               (mapv f))
          (filter-fn target)
          (f target))))

(defn- delete-path!
  [^java.io.File file]
  (let [n (if (.isDirectory file)
            (reduce + (map delete-path! (.listFiles file)))
            0)]
    (io/delete-file file)
    (inc n)))

(defn delete-path "递归删除路径"
  [^java.io.File file]
  (if (.exists file) (delete-path! file) 0))

(defn relative-path "获取相对路径"
  [root child]
  (let [root-file (io/file root)]
    (when-not (.isFile root-file)
      (let [root-path (.getCanonicalPath root-file)
            child-path (.getCanonicalPath (io/file child))]
        (if (= root-path child-path) "."
          (when (clojure.string/starts-with? child-path root-path)
            (subs child-path (inc (count root-path)))))))))

(defn pretty-string "将数据转换成可打印字符串"
  [data {:keys [width zp-option]}]
  (if (pos-int? width)
    (zp/zprint-str data width zp-option)
    (zp/zprint-str data zp-option)))

(defn pretty-write-to-file "输出数据到文件中"
  [data filename {:keys [comparator] :as options}]
  (let [file (io/file filename)
        text (-> (u.core/transform-if data
                                      (constantly (some? comparator))
                                      #(u.core/transform-if (sort comparator %)
                                                            (constantly (vector? data))
                                                            vec))
                 (pretty-string options))]
    (io/make-parents file)
    (spit file text)
    (.getPath file)))
