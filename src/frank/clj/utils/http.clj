(ns frank.clj.utils.http
  (:require [clj-http.client :as http]
            [frank.clj.utils.log :as u.log]))

(defn show-response
  "打印请求返回信息"
  [response]
  (if (= 200 (:status response))
    (:body response)
    (u.log/log :debug "response failed" response)))

(defn make-req
  [method url params]
  (cond-> {:url    url
           :method (or method :post)
           :as     :clojure}
          (some? params)
          (assoc :body (str params))))

(defn request
  [req]
  (try
    (-> {:method       :post
         :conn-timeout 2000
         :content-type :application/edn
         :accept       :application/edn}
        (merge req)
        (http/request)
        (show-response))
    (catch Exception e (u.log/log :error e))))

(defn http-post [url params] (request (make-req :post url params)))
(defn http-put [url params] (request (make-req :put url params)))
(defn http-get [url params] (request (make-req :get url params)))
