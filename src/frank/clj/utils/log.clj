(ns frank.clj.utils.log
  (:require [taoensso.timbre :as t.timbre]
            [taoensso.encore :as t.encore])
  (:import (java.util TimeZone)))


(def log-config
  {:timestamp-opts {:pattern  "yyyy-MM-dd HH:mm:ss.SSS"
                    :locale   :jvm-default,
                    :timezone (TimeZone/getDefault)}
   :output-fn      (fn output-fn
                     ([data] (output-fn nil data))
                     ([opts data]
                      (let [{:keys [no-stacktrace? stacktrace-fonts]} opts
                            {:keys [level ?err msg_ ?ns-str ?file
                                    timestamp_ ?line #_hostname_ #_vargs]} data]
                        (str
                          ;; timestamp
                          (force timestamp_) " "
                          ;; log level
                          (clojure.string/upper-case (name level)) " "
                          ;; namespace and line
                          "[" (or ?ns-str ?file "?") ":" (or ?line "?") "]-> "
                          ;; msg
                          (force msg_)
                          ;; stack trace
                          (when-not no-stacktrace?
                            (when-let [err ?err]
                              (str t.encore/system-newline (t.timbre/stacktrace err opts))))))))})

(defmacro log [level & args]
  `(let [level# ~level]
     (t.timbre/with-merged-config log-config
       (if (contains? #{:trace :debug :info :warn :error :fatal :report} level#)
         (t.timbre/log level# ~@args)
         (t.timbre/log :info level# ~@args)))))

(defn show-value
  ([x] (log :info x) x)
  ([msg x] (log :info msg x) x))
