(ns frank.clj.utils.core
  (:require [cheshire.core :as cheshire])
  (:import (java.security MessageDigest)
           (java.time.format DateTimeFormatter)
           (java.time ZoneId Instant LocalDateTime)))


(def ^:private global-id (atom 0))
(def ^:private global-random-generator (java.util.Random.))

(defn now [] (System/currentTimeMillis))

(defn get-id []
  (swap! global-id inc))

(defn assert-ex [express msg data]
  (when-not express (throw (ex-info msg data))))

(defn file->edn [file]
  (clojure.edn/read-string (slurp file)))

(defn env->config
  [variable]
  (some-> variable
          (System/getenv)
          (clojure.edn/read-string)))

(defn split-n [coll n]
  (let [total (count coll)
        batch (->> (if (= 0 (mod total n)) 0 1)
                   (+ (quot total n)))]
    (partition-all batch coll)))

(defn md5-string
  [^String s]
  (try
    (str (reduce (fn [sb b] (let [v (Integer/toHexString (bit-and b 0xff))]
                              (when (== 1 (.length v)) (.append sb "0"))
                              (.append sb v)))
                 (StringBuilder.)
                 (.digest (MessageDigest/getInstance "md5") (.getBytes s "GBK"))))
    (catch Exception e (.printStackTrace e))))

(def edn->json cheshire/generate-string)
(def json->edn cheshire/parse-string)

(defn millis->string
  ([millis pattern zone-id]
   (-> (DateTimeFormatter/ofPattern pattern)
       (.withZone zone-id)
       (.format (Instant/ofEpochMilli millis))))
  ([millis pattern]
   (millis->string millis pattern (ZoneId/systemDefault)))
  ([millis]
   (millis->string millis "yyyy-MM-dd HH:mm:ss.SSS")))

(defn string->millis
  ([string pattern zone-id]
   (-> (LocalDateTime/parse string (DateTimeFormatter/ofPattern pattern))
       (.atZone zone-id)
       (.toInstant)
       (.toEpochMilli)))
  ([string pattern]
   (string->millis string pattern (ZoneId/systemDefault)))
  ([string]
   (string->millis string "yyyy-MM-dd HH:mm:ss.SSS")))

(defn multi-thread
  [n f]
  (dotimes [i n] (.start (Thread. (fn [] (f i))))))

(defn random
  ([]
   (.nextLong global-random-generator))
  ([b]
   (random 0 b))
  ([a b]
   (->> (- b a)
        (mod (random))
        (+ a))))

(defn transform-if
  [data pred transform-fn]
  (if (pred data) (transform-fn data) data))

(defn comparator-with-keys
  [keys]
  (fn [a b] (compare (mapv (partial get a) keys)
                     (mapv (partial get b) keys))))

(defn sort-by-keys
  [coll keys]
  (sort (comparator-with-keys keys) coll))

(defn compare-sequence
  ([a b]
   (compare-sequence = a b))
  ([f a b]
   (let [x (count a)
         y (count b)]
     (if (= x y)
       (let [index (filter (fn [i] (not (f (nth a i) (nth b i)))) (range x))]
         (if (empty? index)
           {:result :equal}
           {:result :different-element :data index}))
       {:result :different-count :data [x y]}))))

(defn index-if
  ([coll f]
   (->> (count coll)
        (range)
        (some (fn [i] (when (f (nth coll i)) i)))))
  ([coll f x]
   (index-if coll (fn [v] (f v x))))
  ([coll f x y]
   (index-if coll (fn [v] (f v x y))))
  ([coll f x y & args]
   (index-if coll (fn [v] (apply f v x y args)))))

(defn index-of
  [coll v]
  (index-if coll (partial = v)))

(defn transform-map
  ([m transform-fn]
   (transform-map m transform-fn (constantly true)))
  ([m transform-fn filter-fn]
   (->> (map (fn [[k v]] [k (transform-fn v)]) m)
        (filter (fn [[k v]] (filter-fn k v)))
        (into {}))))

(defn- take-elements
  [coll]
  (let [f (if (odd? (count coll)) odd? even?)]
    (set (keep-indexed (fn [i v] (when (f i) v)) coll))))

(defn extract-cyclic
  ([m1 m2 path coll]
   (mapcat (fn [k] (if (contains? (take-elements path) k)
                     (when (= k (first path)) [(conj path k)])
                     (when-let [v (get m2 k)]
                       (extract-cyclic m2 m1 (conj path k) v))))
           coll))
  ([m1 m2]
   (mapcat (fn [[k v]] (extract-cyclic m1 m2 [k] v)) m1))
  ([m path coll]
   (mapcat (fn [k] (if (contains? (set path) k)
                     (when (= k (first path)) [(conj path k)])
                     (when-let [v (get m k)]
                       (extract-cyclic m (conj path k) v))))
           coll))
  ([m]
   (mapcat (fn [[k v]] (extract-cyclic m [k] v)) m)))
